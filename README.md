zxvcv/gcc
=========
Image created mainly to work with the gcc compiler
and to work with conan packages for linux OS.

Run docker image in interactive mode
------------------------------------
```
docker run -ti zxvcv/gcc:latest
```

Run docker images in interactive mode as current user:
```
docker run -ti -u $(id -u ${USER}):$(id -g ${USER}) zxvcv/gcc:latest
```

Pushing docker image to dockerhub
---------------------------------
Dockerhub authentication: `docker login`
Push image to dockerhub: `docker push zxvcv/gcc:latest`

Helps
-----
```
service docker start
sudo docker run -ti -w=/home/conan/pckg -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg zxvcv/gcc:latest
docker build -t zxvcv/gcc:latest
```

References:
-----------
- Repository: https://gitlab.com/zxvcv-docker/gcc
- Docker Hub: https://hub.docker.com/repository/docker/zxvcv/gcc
