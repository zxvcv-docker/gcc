# syntax=docker/dockerfile:1
FROM zxvcv/conan:latest
WORKDIR /app

RUN apt-get install -y gcc-11=11.2.0-19ubuntu1
